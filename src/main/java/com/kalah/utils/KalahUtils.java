package com.kalah.utils;


import com.kalah.common.KalahConstants;

import java.util.stream.IntStream;

public class KalahUtils {

    private KalahUtils() {
        throw new IllegalArgumentException("Can't instantiate KalahUtils class");
    }

    public static int[] generateBoard() {
        return IntStream.range(0, KalahConstants.TOTAL_PITS).map(value ->
                value % (KalahConstants.STONES_IN_PIT + 1) == 0 ? 0 : 6
        ).toArray();
    }
}
