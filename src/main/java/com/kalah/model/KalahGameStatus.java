package com.kalah.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.kalah.common.KalahGameStatusDeserializer;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonDeserialize(using = KalahGameStatusDeserializer.class)
public enum KalahGameStatus {
    STARTED("STARTED"),
    STOPPED("STOPPED"),
    FINISHED("FINISHED");

    private final String status;

    KalahGameStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
