package com.kalah.model;

import org.apache.commons.lang3.ArrayUtils;

import java.util.stream.IntStream;

public class KalahPlayer {
    private final int id;
    private final int[] pits;
    private final int ownKalah;
    private final int opponentKalah;
    private final String name;
    private final int startPit;
    private final int endPit;

    private KalahPlayer(Builder builder) {
        this.id = builder.id;
        this.startPit = builder.startPit;
        this.endPit = builder.endPit;
        this.pits = IntStream.rangeClosed(builder.startPit, builder.endPit).toArray();
        this.ownKalah = builder.ownKalah;
        this.opponentKalah= builder.opponentKalah;
        this.name = builder.name;
    }

    public static class Builder {
        private int id;
        private int startPit;
        private int endPit;
        private int ownKalah;
        private int opponentKalah;
        private String name;

        public Builder setId(int id) {
            this.id = id;
            return this;
        }

        public Builder setOwnKalah(int ownKalah) {
            this.ownKalah = ownKalah;
            return this;
        }

        public Builder setOpponentKalah(int opponentKalah) {
            this.opponentKalah = opponentKalah;
            return this;
        }

        public Builder setStartPit(int startPit) {
            this.startPit = startPit;
            return this;
        }

        public Builder setEndPit(int endPit) {
            this.endPit = endPit;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public KalahPlayer build(){
            return new KalahPlayer(this);
        }
    }

    public boolean ownPlayerPits(int pit){
        return ArrayUtils.contains(this.getPits(), pit);
    }

    public int getId() {
        return id;
    }

    public int[] getPits() {
        return pits;
    }

    public int getOwnKalah() {
        return ownKalah;
    }

    public int getOpponentKalah() {
        return opponentKalah;
    }

    public String getName() {
        return name;
    }

    public int getStartPit(){
        return startPit;
    }

    public int getEndPit(){
        return endPit;
    }
}
