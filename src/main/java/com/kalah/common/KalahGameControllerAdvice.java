package com.kalah.common;

import com.kalah.exceptions.EmptyPitException;
import com.kalah.exceptions.GameNotStartedException;
import com.kalah.exceptions.PlayerNotFoundException;
import com.kalah.exceptions.WrongPitException;
import com.kalah.exceptions.WrongPlayerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;

@ControllerAdvice
@RestController
public class KalahGameControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(KalahGameControllerAdvice.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(EmptyPitException.class)
    public ErrorEntity emptyPit(EmptyPitException exception) {
        LOGGER.error("Try to get stones from empty pit", exception);
        return new ErrorEntity(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(PlayerNotFoundException.class)
    public ErrorEntity playerNotFound(PlayerNotFoundException exception) {
        LOGGER.error("Can't find player", exception);
        return new ErrorEntity(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(WrongPitException.class)
    public ErrorEntity wrongPit(WrongPitException exception) {
        LOGGER.error("Can't take stones from opponent pit", exception);
        return new ErrorEntity(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(GameNotStartedException.class)
    public ErrorEntity gameNotStarted(GameNotStartedException exception) {
        LOGGER.error("Can't play in not initialized game", exception);
        return new ErrorEntity(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(WrongPlayerException.class)
    public ErrorEntity GameNotStartedException(WrongPlayerException exception) {
        LOGGER.error("Player can't make two turns", exception);
        return new ErrorEntity(exception.getMessage());
    }

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorEntity wrongArgumentType(MethodArgumentTypeMismatchException e) {
        LOGGER.error("Wrong arguments: ", e.getMessage());
        return new ErrorEntity(String.format("Incorrect value [%s] for parameter '%s'", e.getValue(), e.getName()));
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorEntity handleResourceNotFoundException(ConstraintViolationException e) {
        Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
        StringBuilder builder = new StringBuilder();
        violations.forEach(violation -> builder.append(violation.getMessage()).append("\n"));
        LOGGER.error("Constraint violations ", e.getMessage());
        return new ErrorEntity(builder.substring(0, builder.length() - 1));
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorEntity handleResourceNotFoundException(Throwable exception) {
        LOGGER.error("Unknown server error occurred", exception);
        return new ErrorEntity("Unknown server error");
    }
}