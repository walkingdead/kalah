package com.kalah.common;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.kalah.model.KalahGameStatus;

import java.io.IOException;

public class KalahGameStatusDeserializer extends JsonDeserializer<KalahGameStatus> {
    @Override
    public KalahGameStatus deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        final JsonNode jsonNode = jp.readValueAsTree();
        String status = jsonNode.get("status").asText();
        return KalahGameStatus.valueOf(status);
    }
}
