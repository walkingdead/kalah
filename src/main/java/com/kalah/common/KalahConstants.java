package com.kalah.common;


public final class KalahConstants {

    // Total number of pits on the board (6 + 6 + 1 + 1)
    public static final int TOTAL_PITS = 14;

    // Number of stones in one pit
    public static final int STONES_IN_PIT = 6;

    //Index of first kalah
    public static final int FIRST_KALAH = 0;

    //Index of the second kalah
    public static final int SECOND_KALAH = 7;

    public static final int TOTAL_STONES = 72;

}
