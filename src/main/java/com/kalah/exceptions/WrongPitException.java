package com.kalah.exceptions;


public class WrongPitException extends RuntimeException {

    public WrongPitException() {
        super();
    }

    public WrongPitException(String message) {
        super(message);
    }

    public WrongPitException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongPitException(Throwable cause) {
        super(cause);
    }
}
