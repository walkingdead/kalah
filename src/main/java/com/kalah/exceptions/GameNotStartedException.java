package com.kalah.exceptions;


public class GameNotStartedException extends RuntimeException {

    public GameNotStartedException() {
        super();
    }

    public GameNotStartedException(String message) {
        super(message);
    }

    public GameNotStartedException(String message, Throwable cause) {
        super(message, cause);
    }

    public GameNotStartedException(Throwable cause) {
        super(cause);
    }
}
