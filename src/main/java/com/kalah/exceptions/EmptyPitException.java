package com.kalah.exceptions;


public class EmptyPitException extends RuntimeException {

    public EmptyPitException() {
        super();
    }

    public EmptyPitException(String message) {
        super(message);
    }

    public EmptyPitException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmptyPitException(Throwable cause) {
        super(cause);
    }
}
