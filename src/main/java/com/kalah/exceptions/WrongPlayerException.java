package com.kalah.exceptions;


public class WrongPlayerException extends RuntimeException {

    public WrongPlayerException() {
        super();
    }

    public WrongPlayerException(String message) {
        super(message);
    }

    public WrongPlayerException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongPlayerException(Throwable cause) {
        super(cause);
    }
}
