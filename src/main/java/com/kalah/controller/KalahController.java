package com.kalah.controller;


import com.kalah.common.ErrorEntity;
import com.kalah.common.KalahConstants;
import com.kalah.dto.KalahGameDto;
import com.kalah.dto.KalahPlayerDto;
import com.kalah.service.KalahService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("kalah")
@Validated
public class KalahController {

    private final KalahService kalahService;

    public KalahController(KalahService kalahService) {
        this.kalahService = kalahService;
    }

    @PostMapping("/init")
    public ResponseEntity<KalahGameDto> init() {
        return ResponseEntity.ok(this.kalahService.init());
    }

    @GetMapping("/players")
    public ResponseEntity<List<KalahPlayerDto>> getPlayers() {
        return ResponseEntity.ok(this.kalahService.getPlayers());
    }

    /**
     * Returns new game state after another move
     * Player id and pit are required fields.
     * You can't select pit outside the board and pit which is kalah pit.
     *
     * @param playerId next player id
     * @param pit selected pit
     * @return new game state
     */
    @PutMapping("/move")
    public ResponseEntity move(@RequestParam("playerId")
                               @NotNull(message = "Player Id is required") Integer playerId,
                               @RequestParam("pit")
                               @NotNull(message = "Pit is required") Integer pit) {
        if (pit <= KalahConstants.FIRST_KALAH || pit == KalahConstants.SECOND_KALAH || pit >= KalahConstants.TOTAL_PITS) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorEntity("Incorrect value pit value : " + pit));
        }
        return ResponseEntity.ok(kalahService.nextMove(playerId, pit));
    }
}
