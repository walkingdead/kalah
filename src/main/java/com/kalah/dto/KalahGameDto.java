package com.kalah.dto;


import com.kalah.model.KalahGameStatus;

public class KalahGameDto {


    private int[] board;
    private KalahGameStatus status;
    private int nextPlayerId;

    public KalahGameDto() {
    }

    public KalahGameDto(int[] board, KalahGameStatus status, int nextPlayerId) {
        this.board = board;
        this.status = status;
        this.nextPlayerId = nextPlayerId;
    }

    public int[] getBoard() {
        return board;
    }

    public void setBoard(int[] board) {
        this.board = board;
    }

    public KalahGameStatus getStatus() {
        return status;
    }

    public void setStatus(KalahGameStatus gameStatus) {
        this.status = gameStatus;
    }

    public int getNextPlayerId() {
        return nextPlayerId;
    }

    public void setNextPlayerId(int nextPlayerId) {
        this.nextPlayerId = nextPlayerId;
    }
}
