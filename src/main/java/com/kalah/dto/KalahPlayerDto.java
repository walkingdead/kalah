package com.kalah.dto;

public class KalahPlayerDto {
    private int id;
    private String name;
    private int startPit;
    private int endPit;
    private int kalahPit;

    public KalahPlayerDto() {
    }

    public KalahPlayerDto(int id, String name, int startPit, int endPit, int kalahPit) {
        this.id = id;
        this.name = name;
        this.startPit = startPit;
        this.endPit = endPit;
        this.kalahPit = kalahPit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStartPit() {
        return startPit;
    }

    public void setStartPit(int startPit) {
        this.startPit = startPit;
    }

    public int getEndPit() {
        return endPit;
    }

    public void setEndPit(int endPit) {
        this.endPit = endPit;
    }

    public int getKalahPit() {
        return kalahPit;
    }

    public void setKalahPit(int kalahPit) {
        this.kalahPit = kalahPit;
    }
}
