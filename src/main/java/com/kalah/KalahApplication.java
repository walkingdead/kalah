package com.kalah;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KalahApplication {

    private static final Logger logger = LoggerFactory.getLogger(KalahApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(KalahApplication.class);
        logger.info("Application Kalah has started");
    }

}
