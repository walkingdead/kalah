package com.kalah.service;

import com.google.common.collect.ImmutableMap;
import com.kalah.common.KalahConstants;
import com.kalah.dto.KalahGameDto;
import com.kalah.dto.KalahPlayerDto;
import com.kalah.exceptions.GameNotStartedException;
import com.kalah.exceptions.PlayerNotFoundException;
import com.kalah.model.KalahGameStatus;
import com.kalah.model.KalahPlayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Service
public class KalahService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KalahService.class);

    private KalahGame kalahGame;

    private KalahPlayer minPlayer;
    private KalahPlayer maxPlayer;

    private static Map<Integer, KalahPlayer> PLAYERS;

    public KalahService() {
        minPlayer = new KalahPlayer.Builder().setId(1)
                .setStartPit(1)
                .setName("Min player")
                .setEndPit(6)
                .setOwnKalah(KalahConstants.SECOND_KALAH)
                .setOpponentKalah(KalahConstants.FIRST_KALAH)
                .build();

        maxPlayer = new KalahPlayer.Builder().setId(2)
                .setStartPit(8)
                .setName("Max player")
                .setEndPit(13)
                .setOwnKalah(KalahConstants.FIRST_KALAH)
                .setOpponentKalah(KalahConstants.SECOND_KALAH)
                .build();

        PLAYERS = ImmutableMap.<Integer, KalahPlayer>builder()
                .put(minPlayer.getId(), minPlayer)
                .put(maxPlayer.getId(), maxPlayer)
                .build();
    }

    /**
     * Initialize game - default board, started game status, next player is Min Player
     *
     * @return inital state of game
     */
    public KalahGameDto init() {
        kalahGame = new KalahGame();
        return new KalahGameDto(kalahGame.getBoard(),
                KalahGameStatus.STARTED,
                1);
    }

    /**
     * Performs next move in game
     * Throws:
     *  - PlayerNotFoundException if player id is not in (MIN_PLAYER, MAX_PLAYER)
     *  - GameNotStartedException if you want start to play but not initialize game
     *
     * @param playerId cucrrent player id
     * @param pit selected pit
     * @return new state of game
     */
    public KalahGameDto nextMove(Integer playerId, int pit) {
        KalahPlayer kalahPlayer = PLAYERS.get(playerId);
        if (kalahPlayer == null) {
            throw new PlayerNotFoundException("Player with id " + playerId + " not found");
        }

        if (this.kalahGame == null || this.kalahGame.getGameStatus() != KalahGameStatus.STARTED) {
            throw new GameNotStartedException("Can't play in not initialized game");
        }

        KalahGameDto kalahGameState = this.kalahGame.nextMove(pit, kalahPlayer);
        if (kalahGameState.getStatus() == KalahGameStatus.FINISHED) {
            this.finishGame(playerId, kalahGameState.getBoard());
        }

        return kalahGameState;
    }

    /**
     * Gets list of player
     *
     * @return list of players
     */
    public List<KalahPlayerDto> getPlayers() {
        return PLAYERS.values().stream().map(player ->
                new KalahPlayerDto(player.getId(),
                        player.getName(),
                        player.getStartPit(),
                        player.getEndPit(),
                        player.getOwnKalah()))
                .collect(toList());
    }

    public KalahGame getKalahGame() {
        return kalahGame;
    }

    /**
     * Finishes the game. If some stones are left they are moved to opponent kalah
     * Clears all pits except of kalah pits.
     *
     * @param playerId last player id
     * @param board current board
     */
    private void finishGame(int playerId, int[] board) {
        LOGGER.info("Finish game playerId {}, board", playerId, board);
        KalahPlayer opponent = PLAYERS.get(3 - playerId);
        board[opponent.getOwnKalah()] = KalahConstants.TOTAL_STONES - board[PLAYERS.get(playerId).getOwnKalah()];
        for (int pit = 1; pit < KalahConstants.TOTAL_PITS; pit++) {
            if (pit != KalahConstants.SECOND_KALAH) {
                board[pit] = 0;
            }
        }
    }
}
