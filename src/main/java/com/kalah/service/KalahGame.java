package com.kalah.service;

import com.kalah.common.KalahConstants;
import com.kalah.dto.KalahGameDto;
import com.kalah.exceptions.EmptyPitException;
import com.kalah.exceptions.WrongPitException;
import com.kalah.exceptions.WrongPlayerException;
import com.kalah.model.KalahGameStatus;
import com.kalah.model.KalahPlayer;
import com.kalah.utils.KalahUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * This class implements all game logic
 */
public class KalahGame {

    private static final Logger LOGGER = LoggerFactory.getLogger(KalahService.class);

    private int[] board;
    // We start game with first player
    private int nextPlayerId = 1;
    // When game is created set status started
    private final KalahGameStatus gameStatus = KalahGameStatus.STARTED;

    public KalahGame() {
        int arr[] = KalahUtils.generateBoard();
        this.board = Arrays.copyOf(arr, arr.length);
    }

    /**
     * Returns copy of current kalah board
     * @return copy of current board
     */
    public int[] getBoard() {
        return Arrays.copyOf(board, board.length);
    }

    /**
     * For test purposes only
     *
     * @param board custom board
     */
    public void setBoard(int[] board) {
        this.board = Arrays.copyOf(board, board.length);
    }

    public KalahGameStatus getGameStatus() {
        return gameStatus;
    }

    /**
     * Method performs a sinle move for a current player
     * Throws:
     *  - WrongPlayerException if player try to make a move more than once
     *  - WrongPitException if player try to take stones from opponent pit
     *  - EmptyPitException if player try to grab stones from empty pit
     *
     * @param pit selected pit
     * @param kalahPlayer current play that will make next move
     * @return new game state
     */

    public KalahGameDto nextMove(int pit, KalahPlayer kalahPlayer) {
        if (kalahPlayer.getId() != nextPlayerId) {
            throw new WrongPlayerException(String.format("Next move player %d but found %d", nextPlayerId, kalahPlayer.getId()));
        }

        int stones = board[pit];

        if (!kalahPlayer.ownPlayerPits(pit)) {
            throw new WrongPitException("Pit " + pit + " is not allowed for player " + kalahPlayer.getName());
        }

        if (stones == 0) {
            throw new EmptyPitException("Can't take anything from empty pit");
        }
        boolean canCaptureOpponentStones = tryToCaptureOpponentStones(kalahPlayer, pit);
        this.nextPlayerId = getNextPlayerId(kalahPlayer, pit);
        moveStones(kalahPlayer, pit);

        if (canCaptureOpponentStones) {
            LOGGER.info("Capture opponents stones playerId {}", kalahPlayer.getId());
            int nextPit = pit;
            if (pit + stones >= KalahConstants.TOTAL_PITS) {
                nextPit = 2 * KalahConstants.TOTAL_PITS - stones - pit - 1;
            }
            grabOpponentStones(kalahPlayer, KalahConstants.TOTAL_PITS - nextPit, nextPit);
        }

        return new KalahGameDto(Arrays.copyOf(board, board.length),
                getGameStatus(kalahPlayer),
                nextPlayerId);
    }

    /**
     * Moves stones one by one from selected pit.
     * After putting all stones clears current pit
     *
     * @param kalahPlayer current player
     * @param pit selected pit
     */
    private void moveStones(KalahPlayer kalahPlayer, int pit) {
        int stones = board[pit];
        int newPit = pit;
        for (int i = 1; i <= stones; i++) {
            if (newPit + 1 < KalahConstants.TOTAL_PITS) {
                newPit = putStoneToPit(kalahPlayer, newPit + 1);
            } else {
                newPit = putStoneToPit(kalahPlayer, newPit + 1 - KalahConstants.TOTAL_PITS);
            }
        }

        board[pit] = 0;
    }

    /**
     * Moves a single stone to adjacent pit.
     * If next pit is opponent kalah - skip it.
     *
     * @param kalahPlayer current player
     * @param currentPit selected pit
     * @return
     */
    private int putStoneToPit(KalahPlayer kalahPlayer, int currentPit) {
        if (currentPit == kalahPlayer.getOpponentKalah()) {
            board[currentPit + 1]++;
            return currentPit + 1;
        } else {
            board[currentPit]++;
            return currentPit;
        }
    }

    /**
     * Tries to grab opponents stones. This could happen if last stone will be put to
     * empty own pit. In this case player can grab all stone from opposite opponent pit
     *
     * @param kalahPlayer current player
     * @param pit selected pit
     * @return
     */
    private boolean tryToCaptureOpponentStones(KalahPlayer kalahPlayer, int pit) {
        int stones = board[pit];
        int nextPitBeforeMaximum = pit + stones;
        int nextPitAfterMaximum = pit + stones + 1 - KalahConstants.TOTAL_PITS;
        if (pit + stones < KalahConstants.TOTAL_PITS) {
            return checkOwnAndComponentPits(kalahPlayer, nextPitBeforeMaximum);
        } else {
            return checkOwnAndComponentPits(kalahPlayer, nextPitAfterMaximum);
        }
    }

    /**
     * Checks if your pit is your's, it's empty and opponent opposite pit is not empty
     *
     * @param kalahPlayer current player
     * @param nextPit next pit
     * @return
     */
    private boolean checkOwnAndComponentPits(KalahPlayer kalahPlayer, int nextPit) {
        return kalahPlayer.ownPlayerPits(nextPit)
                && board[nextPit] == 0
                && board[KalahConstants.TOTAL_PITS - nextPit] > 0;
    }

    /**
     * Grabs stones from current player and opponent player pit and put to current player kalah.
     * Clears both current and opponent pits.
     *
     * @param kalahPlayer current player
     * @param pit current player pit
     * @param opponentPit opponent pit
     */
    private void grabOpponentStones(KalahPlayer kalahPlayer, int pit, int opponentPit) {
        board[kalahPlayer.getOwnKalah()] += board[pit] + board[opponentPit];
        board[pit] = 0;
        board[opponentPit] = 0;
    }

    /**
     * Checks game status (FINISHED or STARTED)
     *
     * @param kalahPlayer current player
     * @return game status
     */
    private KalahGameStatus getGameStatus(KalahPlayer kalahPlayer) {
        boolean gameStatus = checkGameOver(kalahPlayer);
        LOGGER.info("Game status {}", gameStatus);
        return gameStatus ? KalahGameStatus.FINISHED : KalahGameStatus.STARTED;
    }

    /**
     * Checks if all pits of current player are empty
     *
     * @param kalahPlayer current player
     * @return true if game is finished
     */
    private boolean checkGameOver(KalahPlayer kalahPlayer) {
        return
                IntStream.rangeClosed(0, 5)
                        .filter(value -> board[kalahPlayer.getPits()[value]] == 0)
                        .count() == KalahConstants.STONES_IN_PIT;
    }


    /**
     * Gets next player
     *
     * @param kalahPlayer current player
     * @param pit selected pit
     * @return
     */
    private int getNextPlayerId(KalahPlayer kalahPlayer, int pit) {
        int stones = board[pit];
        if (pit + stones < KalahConstants.TOTAL_PITS) {
            return calculateNextPlayerId(kalahPlayer, pit + stones);
        } else {
            return calculateNextPlayerId(kalahPlayer, pit + stones - KalahConstants.TOTAL_PITS);
        }
    }

    /**
     * Calculates next player id. If last pit was player kalah pit he has a chance of
     * extra move.
     *
     * @param kalahPlayer current player
     * @param calcuatedPit new pit
     * @return next player id
     */
    private int calculateNextPlayerId(KalahPlayer kalahPlayer, int calcuatedPit) {
        if (calcuatedPit == kalahPlayer.getOwnKalah()) {
            return kalahPlayer.getId();
        }
        return 3 - kalahPlayer.getId();
    }
}
