package com.kalah.common.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

@Profile("test")
@ComponentScan(basePackages = "com.kalah")
public class IntegrationTestConfiguration {
}
