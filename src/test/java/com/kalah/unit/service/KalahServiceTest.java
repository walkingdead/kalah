package com.kalah.unit.service;


import com.kalah.common.annotations.UnitTestRunner;
import com.kalah.dto.KalahGameDto;
import com.kalah.dto.KalahPlayerDto;
import com.kalah.exceptions.GameNotStartedException;
import com.kalah.exceptions.PlayerNotFoundException;
import com.kalah.exceptions.WrongPlayerException;
import com.kalah.model.KalahGameStatus;
import com.kalah.service.KalahService;
import com.kalah.utils.TestUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@UnitTestRunner
public class KalahServiceTest {

    private KalahService kalahService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void init() {
        this.kalahService = new KalahService();
    }

    @Test
    public void gameInitTest() {
        KalahGameDto initialGameStatus = kalahService.init();
        assertThat("Initial board", initialGameStatus.getBoard(), is(TestUtils.getInitialBoard()));
        assertThat("Game status", initialGameStatus.getStatus(), is(KalahGameStatus.STARTED));
        assertThat("Next player id", initialGameStatus.getNextPlayerId(), is(1));
    }

    @Test
    public void getPlayersTest() {
        List<KalahPlayerDto> players = kalahService.getPlayers();
        assertThat("Players number", players.size(), is(2));
        Map<Integer, KalahPlayerDto> playersMap = players.stream()
                .collect(Collectors.toMap(KalahPlayerDto::getId, Function.identity()));

        KalahPlayerDto minPlayer = playersMap.get(1);
        KalahPlayerDto maxPlayer = playersMap.get(2);

        assertThat("First player name", minPlayer.getName(), is("Min player"));
        assertThat("First player start pit", minPlayer.getStartPit(), is(1));
        assertThat("First player end pit", minPlayer.getEndPit(), is(6));

        assertThat("Second player name", maxPlayer.getName(), is("Max player"));
        assertThat("Second player start pit", maxPlayer.getStartPit(), is(8));
        assertThat("Second player end pit", maxPlayer.getEndPit(), is(13));
    }

    @Test
    public void nextMovePlayersNotFoundTest() {
        expectedException.expect(PlayerNotFoundException.class);
        expectedException.expectMessage("Player with id 3 not found");
        kalahService.nextMove(3, 5);
    }

    @Test
    public void nextMoveGameNotStartedTest() {
        expectedException.expect(GameNotStartedException.class);
        expectedException.expectMessage("Can't play in not initialized game");
        kalahService.nextMove(1, 5);
    }

    @Test
    public void nextMoveSamePlayer() {
        expectedException.expect(WrongPlayerException.class);
        expectedException.expectMessage("Next move player 2 but found 1");
        kalahService.init();
        kalahService.nextMove(1, 5);
        kalahService.nextMove(1, 5);
    }

    @Test
    public void nextMoveTest() {
        kalahService.init();
        KalahGameDto gameState = kalahService.nextMove(1, 2);
        assertThat("Game status", gameState.getStatus(), is(KalahGameStatus.STARTED));
        assertThat("Board status", gameState.getBoard(),
                is(new int[]{0, 6, 0, 7, 7, 7, 7, 1, 7, 6, 6, 6, 6, 6}));
        assertThat("next player id", gameState.getNextPlayerId(), is(2));
    }

    @Test
    public void nextMoveStoneInOwnKalah() {
        kalahService.init();
        kalahService.nextMove(1, 2);
        kalahService.getKalahGame().setBoard(new int[]{0, 6, 6, 6, 9, 6, 6, 0, 6, 6, 6, 3, 6, 6});
        KalahGameDto gameState = kalahService.nextMove(2, 11);
        assertThat("Game status", gameState.getStatus(), is(KalahGameStatus.STARTED));
        assertThat("Board status", gameState.getBoard(),
                is(new int[]{1, 6, 6, 6, 9, 6, 6, 0, 6, 6, 6, 0, 7, 7}));
        assertThat("next player id", gameState.getNextPlayerId(), is(2));
    }

    @Test
    public void nextMoveStoneGrabOpponentsStones() {
        kalahService.init();
        kalahService.getKalahGame().setBoard(new int[]{3, 6, 0, 6, 6, 6, 9, 3, 6, 6, 6, 3, 6, 6});
        KalahGameDto gameState = kalahService.nextMove(1, 6);
        assertThat("Game status", gameState.getStatus(), is(KalahGameStatus.STARTED));
        assertThat("Board status", gameState.getBoard(),
                is(new int[]{3, 7, 0, 6, 6, 6, 0, 12, 7, 7, 7, 4, 0, 7}));
        assertThat("next player id", gameState.getNextPlayerId(), is(2));
    }

    @Test
    public void nextMoveFinishGame() {
        kalahService.init();
        kalahService.getKalahGame().setBoard(new int[]{25, 0, 0, 0, 0, 0, 1, 29, 4, 0, 3, 2, 1, 6});
        KalahGameDto gameState = kalahService.nextMove(1, 6);
        assertThat("Game status", gameState.getStatus(), is(KalahGameStatus.FINISHED));
        assertThat("Board status", gameState.getBoard(),
                is(new int[]{42, 0, 0, 0, 0, 0, 0, 30, 0, 0, 0, 0, 0, 0}));
        assertThat("next player id", gameState.getNextPlayerId(), is(1));
    }

}