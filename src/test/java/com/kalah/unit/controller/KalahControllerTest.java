package com.kalah.unit.controller;

import com.kalah.common.annotations.UnitTestRunner;
import com.kalah.controller.KalahController;
import com.kalah.dto.KalahGameDto;
import com.kalah.dto.KalahPlayerDto;
import com.kalah.exceptions.EmptyPitException;
import com.kalah.exceptions.GameNotStartedException;
import com.kalah.exceptions.PlayerNotFoundException;
import com.kalah.exceptions.WrongPitException;
import com.kalah.model.KalahGameStatus;
import com.kalah.service.KalahService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(KalahController.class)
@UnitTestRunner
public class KalahControllerTest {

    @MockBean
    private KalahService kalahService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testInit() throws Exception {
        when(kalahService.init()).thenReturn(
                new KalahGameDto(new int[]{0, 6, 6, 6, 0, 6, 6, 6},
                        KalahGameStatus.STARTED, 1));
        this.mockMvc.perform(post("/kalah/init"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(
                        "{\"board\":[0,6,6,6,0,6,6,6],\"status\":{\"status\":\"STARTED\"},\"nextPlayerId\":1}"
                ));
    }

    @Test
    public void getPlayers() throws Exception {
        when(kalahService.getPlayers()).thenReturn(
                Arrays.asList(new KalahPlayerDto(1, "Test player 1",
                                1, 6, 7),
                        new KalahPlayerDto(2, "Test player 2", 7, 13, 0)));
        this.mockMvc.perform(get("/kalah/players"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(
                        "[{\"id\":1,\"name\":\"Test player 1\",\"startPit\":1,\"endPit\":6,\"kalahPit\":7}," +
                                "{\"id\":2,\"name\":\"Test player 2\",\"startPit\":7,\"endPit\":13,\"kalahPit\":0}]"
                ));
    }

    @Test
    public void nextMoveSuccess() throws Exception {
        when(kalahService.nextMove(1, 2)).thenReturn(
                new KalahGameDto(new int[]{1, 5, 7, 4, 7, 8, 1, 3, 4}, KalahGameStatus.STARTED, 2));
        this.mockMvc.perform(put("/kalah/move?playerId=1&pit=2"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(
                        "{\"board\":[1,5,7,4,7,8,1,3,4],\"status\":{\"status\":\"STARTED\"},\"nextPlayerId\":2}"
                ));
    }

    @Test
    public void playerNotFound() throws Exception {
        when(kalahService.nextMove(1, 2)).thenThrow(new PlayerNotFoundException("Player with id 2 not found"));
        this.mockMvc.perform(put("/kalah/move?playerId=1&pit=2"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string("{\"message\":\"Player with id 2 not found\"}"
                ));
    }

    @Test
    public void wrongPitException() throws Exception {
        when(kalahService.nextMove(1, 2)).thenThrow(new WrongPitException("Pit 2 is not allowed for player 1"));
        this.mockMvc.perform(put("/kalah/move?playerId=1&pit=2"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string("{\"message\":\"Pit 2 is not allowed for player 1\"}"
                ));
    }

    @Test
    public void emptyPitException() throws Exception {
        when(kalahService.nextMove(1, 2)).thenThrow(new EmptyPitException("Pit 2 has no stones"));
        this.mockMvc.perform(put("/kalah/move?playerId=1&pit=2"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string("{\"message\":\"Pit 2 has no stones\"}"
                ));
    }

    @Test
    public void gameNotStartedException() throws Exception {
        when(kalahService.nextMove(1, 2)).thenThrow(new GameNotStartedException("Can't play in not initialized game"));
        this.mockMvc.perform(put("/kalah/move?playerId=1&pit=2"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string("{\"message\":\"Can't play in not initialized game\"}"
                ));
    }

    @Test
    public void nextMovePlayerIdIsNull() throws Exception {
        this.mockMvc.perform(put("/kalah/move")
                .param("playerId", "")
                .param("pit", "4"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(
                        "{\"message\":\"Player Id is required\"}"
                ));
    }

    @Test
    public void nextMovePitIsNull() throws Exception {
        this.mockMvc.perform(put("/kalah/move")
                .param("playerId", "1")
                .param("pit", ""))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(
                        "{\"message\":\"Pit is required\"}"
                ));
    }

    @Test
    public void nextMoveIncorrectPitValue() throws Exception {
        this.mockMvc.perform(put("/kalah/move")
                .param("playerId", "1")
                .param("pit", "15"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(
                        "{\"message\":\"Incorrect value pit value : 15\"}"
                ));
    }
}
