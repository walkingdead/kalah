package com.kalah.utils;


import com.kalah.common.KalahConstants;

import java.util.stream.IntStream;

public class TestUtils {

    public static int[] getInitialBoard() {
        return IntStream.range(0, KalahConstants.TOTAL_PITS).map(value ->
                value % (KalahConstants.STONES_IN_PIT + 1) == 0 ? 0 : 6
        ).toArray();
    }

    public static int[] getPlayerPits(int start, int end) {
        return IntStream.range(start, end).map(value -> value == start ? 0 : 6).toArray();
    }
}
