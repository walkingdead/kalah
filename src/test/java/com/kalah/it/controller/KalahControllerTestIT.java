package com.kalah.it.controller;

import com.kalah.common.annotations.FunctionalTestRunner;
import com.kalah.dto.KalahGameDto;
import com.kalah.dto.KalahPlayerDto;
import com.kalah.model.KalahGameStatus;
import com.kalah.utils.TestUtils;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@FunctionalTestRunner
public class KalahControllerTestIT {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${local.server.port}")
    private String port;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void initGameTest() {
        ResponseEntity<KalahGameDto> response = restTemplate.exchange(
                "http://localhost:{port}/kalah/init",
                HttpMethod.POST, null, new ParameterizedTypeReference<KalahGameDto>() {
                }, port);

        assertThat("Init game response code", response.getStatusCode(), is(HttpStatus.OK));
        KalahGameDto kalahGameDto = response.getBody();
        assertThat("Game status", kalahGameDto.getStatus(), is(KalahGameStatus.STARTED));
        assertThat("Next player id", kalahGameDto.getNextPlayerId(), is(1));
        assertThat("Initial board", kalahGameDto.getBoard(), is(TestUtils.getInitialBoard()));
    }

    @Test
    public void getPlayersTest() {
        ResponseEntity<List<KalahPlayerDto>> response = restTemplate.exchange(
                "http://localhost:{port}/kalah/players",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<KalahPlayerDto>>() {
                }, port);

        assertThat("Get players response code", response.getStatusCode(), is(HttpStatus.OK));
        List<KalahPlayerDto> players = response.getBody();
        Assert.assertThat("Players number", players.size(), is(2));

        Map<Integer, KalahPlayerDto> playersMap = players.stream()
                .collect(Collectors.toMap(KalahPlayerDto::getId, Function.identity()));

        KalahPlayerDto minPlayer = playersMap.get(1);
        Assert.assertThat("First player name", minPlayer.getName(), is("Min player"));
        Assert.assertThat("First player end pit", minPlayer.getEndPit(), is(6));

        KalahPlayerDto maxPlayer = playersMap.get(2);
        Assert.assertThat("Second player name", maxPlayer.getName(), is("Max player"));
        Assert.assertThat("Second player start pit", maxPlayer.getStartPit(), is(8));
    }

    @Test
    public void nextMove() {
        initGame();
        ResponseEntity<KalahGameDto> responseFirstMove = restTemplate.exchange(
                "http://localhost:{port}/kalah/move?playerId={playerId}&pit={pit}",
                HttpMethod.PUT, null, new ParameterizedTypeReference<KalahGameDto>() {
                }, port, 1, 3);

        assertThat("Next move first step", responseFirstMove.getStatusCode(), is(HttpStatus.OK));
        KalahGameDto kalahGameFirstMove = responseFirstMove.getBody();
        assertThat("Game board", kalahGameFirstMove.getBoard(),
                is(new int[]{0, 6, 6, 0, 7, 7, 7, 1, 7, 7, 6, 6, 6, 6}));
        assertThat("Next player id", kalahGameFirstMove.getNextPlayerId(), is(2));

        ResponseEntity<KalahGameDto> responseSecondMove = restTemplate.exchange(
                "http://localhost:{port}/kalah/move?playerId={playerId}&pit={pit}",
                HttpMethod.PUT, null, new ParameterizedTypeReference<KalahGameDto>() {
                }, port, 2, 9);

        assertThat("Next move second step", responseFirstMove.getStatusCode(), is(HttpStatus.OK));
        KalahGameDto kalahGameSecondMove = responseSecondMove.getBody();
        assertThat("Game board", kalahGameSecondMove.getBoard(),
                is(new int[]{1, 7, 7, 0, 7, 7, 7, 1, 7, 0, 7, 7, 7, 7}));
        assertThat("Next player id", kalahGameSecondMove.getNextPlayerId(), is(1));
    }

    @Test
    public void nextMoveNonExistingPlayer() {
        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("400 Bad Request");
        restTemplate.exchange(
                "http://localhost:{port}/kalah/move?playerId={playerId}&pit={pit}",
                HttpMethod.PUT, null, new ParameterizedTypeReference<KalahGameDto>() {
                }, port, 3, 1);
    }

    @Test
    public void nextMovePlayerIdIsNull() {
        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("400 Bad Request");

        restTemplate.exchange(
                "http://localhost:{port}/kalah/move?playerId={playerId}&pit={pit}",
                HttpMethod.PUT, null, new ParameterizedTypeReference<KalahGameDto>() {
                }, port, "", 1);
    }

    @Test
    public void nextMovePitIsIncorrect() {
        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("400 Bad Request");
        initGame();
        restTemplate.exchange(
                "http://localhost:{port}/kalah/move?playerId={playerId}&pit={pit}",
                HttpMethod.PUT, null, new ParameterizedTypeReference<KalahGameDto>() {
                }, port, "1", "-10");
    }

    @Test
    public void nextMoveRepeatedBySamePlayer () {
        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("400 Bad Request");
        initGame();
        restTemplate.exchange(
                "http://localhost:{port}/kalah/move?playerId={playerId}&pit={pit}",
                HttpMethod.PUT, null, new ParameterizedTypeReference<KalahGameDto>() {
                }, port, 1, 4);
        restTemplate.exchange(
                "http://localhost:{port}/kalah/move?playerId={playerId}&pit={pit}",
                HttpMethod.PUT, null, new ParameterizedTypeReference<KalahGameDto>() {
                }, port, 1, 4);
    }

    private void initGame(){
        restTemplate.exchange(
                "http://localhost:{port}/kalah/init",
                HttpMethod.POST, null, new ParameterizedTypeReference<KalahGameDto>() {
                }, port);
    }
}
