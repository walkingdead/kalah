How to start application.

Basically you have two options 

 1) Open your project inside
 IDE and launch `com.kalah.KalahApplication` and specify VM options
 parameter -Dspring.profiles.active=dev

 2) - execute command ./gradlew build
    - launch application with following command
    `java -Dspring.profiles.active=dev -jar /build/libs/kalah-1.0-SNAPSHOT.jar`